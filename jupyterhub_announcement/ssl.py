import os

from jupyterhub.utils import make_ssl_context

from traitlets import Unicode
from traitlets.config import Configurable


class SSLContext(Configurable):

    keyfile = Unicode(
            "",
            help="SSL key, use with certfile"
    ).tag(config=True)

    certfile = Unicode(
            "",
            help="SSL cert, use with keyfile"
    ).tag(config=True)

    cafile = Unicode(
            "",
            help="SSL CA, use with keyfile and certfile"
    ).tag(config=True)

    def ssl_context(self):
        if self.keyfile and self.certfile and self.cafile:
            return make_ssl_context(self.keyfile, self.certfile,
                                    cafile=self.cafile, check_hostname=False)
        else:
            return None
