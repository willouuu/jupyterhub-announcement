#!/usr/bin/env python

import os
from setuptools import setup, find_packages
import versioneer

HERE = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(HERE, "README.md"), encoding="utf-8") as f:
    long_desc = f.read()

# setuptools requirements
install_requires = []
with open("requirements.txt") as f:
    for line in f.readlines():
        req = line.strip()
        if not req or req.startswith(("-e", "#")):
            continue
        install_requires.append(req)

setup(
    name="jupyterhub-announcement",
    packages=find_packages(),

    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),

    description="""JupyterHub Documentation Service""",
    long_description=long_desc,
    long_description_content_type="text/markdown",

    author="R. C. Thomas, François Tessier, Mahendra Paipuri",
    author_email='rcthomas@lbl.gov',

    keywords=["Interactive", "Interpreter", "Shell", "Web", "Jupyter"],
    classifiers=[
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],

    entry_points={
        'console_scripts': [
            'jupyterhub_announcement = jupyterhub_announcement.announcement:main',
        ],
    },

    data_files=[
        ("share/jupyterhub/announcement/templates", ["templates/index.html"])
    ],

    install_requires=install_requires,
    include_package_data=True,
    zip_safe=False,
)
