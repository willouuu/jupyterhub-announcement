# Configuration file for jupyterhub.
import os

c.JupyterHub.authenticator_class = 'jupyterhub.auth.DummyAuthenticator'


def default_url_fn(handler):
    user = handler.current_user
    if user and user.admin:
        return '/hub/admin'
    return '/hub/home'


c.JupyterHub.default_url = default_url_fn

api_token = 'my8digitsecret'
c.JupyterHub.services = [
    {
        'name': 'announcement',
        'url': 'http://localhost:8889',
        'api_token': api_token,
        'oauth_no_confirm': True,
    }
]

c.JupyterHub.load_roles = [
    {
      'name': 'user',
      'scopes': ['access:services', 'self'],
      'services': [
            "announcement"
        ],
    }
]

c.Authenticator.admin_users = ['admin']
