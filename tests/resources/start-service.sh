#!/usr/bin/env bash

# Client ID that we need to set
# DO NOT CHANGE THIS. JupyterHub will create a client using
# this ID and it should match with the external service for
# successful authentication
export JUPYTERHUB_CLIENT_ID=service-announcement

# Service prefix
export JUPYTERHUB_SERVICE_PREFIX=/services/announcement/

# Set Hub API URL
export JUPYTERHUB_API_URL=http://localhost:8081/hub/api

# API token that we used in the services definition in the jupyterhub_config.py
# NOTE THAT IT SHOULD ABSOLUTELY MATCH WITH THE ONE PROVIDED IN JUPYTERHUB CONFIG
export JUPYTERHUB_API_TOKEN=my8digitsecret

# Start service
jupyterhub_announcement --AnnouncementService.config_file="announcement_config.py"

