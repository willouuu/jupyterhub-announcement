"""Integration tests"""

import os
import subprocess
import requests


def create_admin_token():
    """Create a token for admin user"""
    # Auth data
    payload = {
        'username': 'admin',
        'password': 'pass',
    }
    # Working directory where JupyterHub DB is located
    db_dir = os.path.join(os.getcwd(), 'tests', 'resources')
    # Open a session to login and create admin user
    with requests.Session() as session:
        auth = session.post("http://localhost:8000/hub/login", data=payload)
        completed = subprocess.run(
            ['jupyterhub', 'token', 'admin'], capture_output=True, check=True,
            cwd=db_dir
        )
    return completed.stdout.decode().splitlines()[-1].strip()


def test_endpoints():
    """Test to check if announcement URL is working"""
    # Auth data
    payload = {
        'username': 'user',
        'password': 'pass',
    }
    # Open a session to login and check announcement URL
    with requests.Session() as session:
        login = session.get("http://localhost:8000/hub/login")
        assert login.status_code == 200

        # Get XSRF session cookie and update payload
        cookie = login.cookies['_xsrf']
        payload.update({
            '_xsrf': cookie
        })

        auth = session.post("http://localhost:8000/hub/login", data=payload)
        assert auth.status_code == 200

        announcement = session.get(
            "http://localhost:8000/services/announcement/"
        )
        assert announcement.status_code == 200
        latest = session.get(
            "http://localhost:8000/services/announcement/latest"
        )
        assert latest.status_code == 200


def test_update_announcement():
    """Test to check if announcement posting is working"""
    token = create_admin_token()
    # Auth data
    payload = {
        'username': 'admin',
        'password': 'pass',
    }
    headers = {
        'Authorization': f'token {token}'
    }
    announcement = {
        'announcement': 'This is test announcement',
        'announcement_until': '2022-12-19T14:30'
    }
    # Open a session to login and check announcement URL
    with requests.Session() as session:
        login = session.get("http://localhost:8000/hub/login")
        assert login.status_code == 200

        # Get XSRF session cookie and update payload
        cookie = login.cookies['_xsrf']
        payload.update({
            '_xsrf': cookie
        })

        auth = session.post("http://localhost:8000/hub/login", data=payload)
        assert auth.status_code == 200
        announcement = session.post(
            "http://localhost:8000/services/announcement/update",
            data=announcement, headers=headers
        )
        assert announcement.status_code == 200
